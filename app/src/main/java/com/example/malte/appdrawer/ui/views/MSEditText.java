package com.example.malte.appdrawer.ui.views;


import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.malte.appdrawer.R;

public class MSEditText extends LinearLayout
{
    public MSEditText(Context _context, AttributeSet _attrs)
    {
        super(_context, _attrs);
        setup(_context, _attrs);
    }

    private void setup(Context _context, AttributeSet _attrs)
    {
        inflate(_context, R.layout.customview_ms_edittext, this);
        setupView();
        setupVariables(_context, _attrs);
        applyVariables();
    }

    private void setupVariables(Context _context, AttributeSet _attrs)
    {
        TypedArray customAttributes = _context.obtainStyledAttributes(_attrs, R.styleable.MSEditText);

        hasBottomBorder = customAttributes.getBoolean(R.styleable.MSEditText_hasBottomBorder, true);
        hint = customAttributes.getString(R.styleable.MSEditText_hint);
        text = customAttributes.getString(R.styleable.MSEditText_text);

        customAttributes.recycle();
    }

    private void setupView()
    {
        LinearLayout mainLinearLayout = (LinearLayout) getChildAt(0);
        LinearLayout secondLinearLayout = (LinearLayout) mainLinearLayout.getChildAt(0);

        bottomBorder = mainLinearLayout.getChildAt(1);
        textView = (TextView) secondLinearLayout.getChildAt(0);
        editText = (EditText) secondLinearLayout.getChildAt(1);
    }

    private void applyVariables()
    {
        bottomBorder.setVisibility(hasBottomBorder ? VISIBLE : INVISIBLE);
        editText.setHint(hint);
        textView.setText(text);
    }

    @NonNull
    public String getText()
    {
        return editText.getText().toString();
    }

    public void deleteInput()
    {
        editText.setText("");
    }

    //View Elements
    private EditText editText;
    private TextView textView;
    private View bottomBorder;
    private boolean hasBottomBorder;
    private String hint;
    private String text;

    private String androidShema = "http://schemas.android.com/apk/res/android";
}
