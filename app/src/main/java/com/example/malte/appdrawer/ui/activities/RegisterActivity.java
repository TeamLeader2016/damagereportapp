package com.example.malte.appdrawer.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.example.malte.appdrawer.R;
import com.example.malte.appdrawer.ui.models.User;
import com.example.malte.appdrawer.ui.storage.UserSharedPreferenceManager;
import com.example.malte.appdrawer.ui.views.MSEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends Activity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.back_button)
    public void onBackButtonPressed()
    {
        setResult(RESULT_CANCELED);
        finish();
    }

    @OnClick(R.id.register_button)
    public void onRegisterButtonPressed()
    {
        if (!passwordMSEditText.getText().equals(passwortRepeatMSEditText.getText())) return;

        User user = new User();
        user.setFirstName(firstNameMSEditText.getText());
        user.setLastName(lastNameMSEditText.getText());
        user.setUserName(usernameMSEditText.getText());
        user.setPassword(passwordMSEditText.getText());
        user.setGender(User.sex.WHAT);
        user.setBirthDate(birthdayMSEditText.getText());

        UserSharedPreferenceManager.putUser(this, UserSharedPreferenceManager.LOGGED_IN_USER, user);

        startActivityForResult(new Intent(this, MainActivity.class), MainActivity.REQUEST_MAIN_ACTIVITY);

        setResult(RESULT_OK);
        
        finish();
    }

    @BindView(R.id.firstname_msedittext) MSEditText firstNameMSEditText;
    @BindView(R.id.lastname_msedittext) MSEditText lastNameMSEditText;
    @BindView(R.id.username_msedittext) MSEditText usernameMSEditText;
    @BindView(R.id.password_msedittext) MSEditText passwordMSEditText;
    @BindView(R.id.password_repeat_msedittext) MSEditText passwortRepeatMSEditText;
    @BindView(R.id.birthday_msedittext) MSEditText birthdayMSEditText;

    public static final int REQUEST_REGISTER_ACTIVITY = 3784;
}
