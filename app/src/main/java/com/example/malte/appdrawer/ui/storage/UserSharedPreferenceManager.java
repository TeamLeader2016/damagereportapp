package com.example.malte.appdrawer.ui.storage;

import android.content.Context;

import com.example.malte.appdrawer.ui.models.User;
import com.google.gson.Gson;

public class UserSharedPreferenceManager extends BasicSharedPreferenceManager
{
    public static void putUser(Context _context, String _key, User _user)
    {
        putString(_context, _key, new Gson().toJson(_user));
    }

    public static User getUser(Context _context, String _key)
    {
        String userString = getString(_context, _key, null);
        return new Gson().fromJson(userString, User.class);
    }

    public static final String LOGGED_IN_USER = "LOGGED_IN_USER";
}
