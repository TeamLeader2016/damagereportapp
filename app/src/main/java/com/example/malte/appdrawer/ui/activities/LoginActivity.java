package com.example.malte.appdrawer.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.malte.appdrawer.R;
import com.example.malte.appdrawer.ui.models.User;
import com.example.malte.appdrawer.ui.storage.UserSharedPreferenceManager;
import com.example.malte.appdrawer.ui.views.MSEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
//Test
public class LoginActivity extends Activity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.back_button)
    public void onBackButtonPressed()
    {
        setResult(RESULT_CANCELED);
        finish();
    }

    @OnClick(R.id.login_button)
    public void onLoginButtonPressed()
    {
        User user = UserSharedPreferenceManager.getUser(this, UserSharedPreferenceManager.LOGGED_IN_USER);

        if (user != null)
        {
            if (user.getUserName().equals(usernameMSEditText.getText()) && user.getPassword().equals(passwordMSEditText.getText()))
            {
                startActivityForResult(new Intent(this, MainActivity.class), MainActivity.REQUEST_MAIN_ACTIVITY);
            }
        }
        else
        {
            Toast.makeText(this, "wrong user", Toast.LENGTH_SHORT).show();
        }
    }

    @BindView(R.id.username_msedittext) MSEditText usernameMSEditText;
    @BindView(R.id.password_msedittext) MSEditText passwordMSEditText;

    public static final int REQUEST_LOGIN_ACTIVITY = 2468;
}
