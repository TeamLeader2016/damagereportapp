package com.example.malte.appdrawer.ui.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

abstract class BasicSharedPreferenceManager
{
    private static SharedPreferences.Editor getEditor(Context _context)
    {
        return _context.getSharedPreferences(SHAREDPREFERENCE_KEY, Context.MODE_PRIVATE).edit();
    }

    private static SharedPreferences getReader(Context _context)
    {
        return _context.getSharedPreferences(SHAREDPREFERENCE_KEY, Context.MODE_PRIVATE);
    }

    protected static void putString(Context _context, String _key, String _value)
    {
        getEditor(_context).putString(_key, _value).apply();
    }

    protected static String getString(Context _context, String _key, String _defaultKey)
    {
        return getReader(_context).getString(_key, _defaultKey);
    }

    private static final String SHAREDPREFERENCE_KEY = "SHAREDPREFERENCE_KEY";
}
