package com.example.malte.appdrawer.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.example.malte.appdrawer.R;
import com.example.malte.appdrawer.ui.storage.UserSharedPreferenceManager;

public class SplashActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setup();
    }

    private void setup()
    {
        setupStart();
    }

    private void setupStart()
    {
        if (UserSharedPreferenceManager.getUser(this, UserSharedPreferenceManager.LOGGED_IN_USER) != null)
        {
            startActivityForResult(new Intent(this, MainActivity.class), MainActivity.REQUEST_MAIN_ACTIVITY);
            finish();
        }
        else
        {
            startActivityForResult(new Intent(this, WelcomeActivity.class), WelcomeActivity.REQUEST_WELCOME_ACTIVITY);
            finish();
        }
    }
}
