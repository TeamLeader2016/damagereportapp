package com.example.malte.appdrawer.ui.models;

/**
 * Created by Malte on 11.01.2017.
 */

public class User
{
    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getBirthDate()
    {
        return birthDate;
    }

    public void setBirthDate(String birthDate)
    {
        this.birthDate = birthDate;
    }

    public sex getGender()
    {
        return gender;
    }

    public void setGender(sex gender)
    {
        this.gender = gender;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    private String userName;
    private String firstName;
    private String lastName;
    private String birthDate;
    private sex gender;
    private String password;

    public enum sex
    {
        MALE, FEMALE, WHAT
    }

    ;
}
