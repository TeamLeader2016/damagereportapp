package com.example.malte.appdrawer.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.example.malte.appdrawer.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class WelcomeActivity extends Activity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch (requestCode)
        {
            case LoginActivity.REQUEST_LOGIN_ACTIVITY:
            {
                if (resultCode == RESULT_OK)
                {
                    finish();
                }
                break;
            }
            case RegisterActivity.REQUEST_REGISTER_ACTIVITY:
            {
                if (resultCode == RESULT_OK)
                {
                    finish();
                }
                break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.login_button)
    public void loginPressed()
    {
        startActivityForResult(new Intent(this, LoginActivity.class), LoginActivity.REQUEST_LOGIN_ACTIVITY);
    }

    @OnClick(R.id.register_button)
    public void registerPressed()
    {
        startActivityForResult(new Intent(this, RegisterActivity.class), RegisterActivity.REQUEST_REGISTER_ACTIVITY);
    }

    public static final int REQUEST_WELCOME_ACTIVITY = 3180;
}
